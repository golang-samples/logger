package logger

import (
	//"fmt"
	"testing"
	"time"
)

func TestFileLogger(t *testing.T) {
	config := make(map[string]string, 8)
	config["log_path"] = "./"
	config["log_name"] = "testlog"
	config["log_level"] = "debug"
	config["log_rotate_type"] = "size"
	//config["log_rotate_size"] = "102400"
	InitLogger("file", config)
	//Debug("debug test.%d %s ", 1, "a")
	//Trace("trace test.%d %s ", 1, "a")
	//Info("info test.%d %s ", 1, "a")
	//Warn("warm test.%d %s ", 1, "a")
	//Error("error test.%d %s ", 1, "a")
	//Fatal("fatal test.%d %s ", 1, "a")
	j := 0
	for {
		j += 1
		Debug("debug test.%d %s ", j, "oooooooooooooewoepweroiwemownrewoa")
		Trace("trace test.%d %s ", j, "qwqwqwqweqweqwreweweqrqra")
		Info("info test.%d %s ", j, "acdcerfwefefwefwefrfrfrf")
		Warn("warm test.%d %s ", j, "aafsafrf223423cdqdfqrfere")
		Error("error test.%d %s ", j, "e2134fl-3lro1m3v1invidnsoivqra")
		Fatal("fatal test.%d %s ", j, "nnnnni4or34ojrolfanfui3rfnrnfna")
	}
	time.Sleep(time.Second * 10)
}

func TestConsoleLogger(t *testing.T) {
	config := make(map[string]string, 8)
	config["log_level"] = "debug"
	InitLogger("console", config)
	Debug("debug test.%d %s ", 1, "a")
	Trace("trace test.%d %s ", 1, "a")
	Info("info test.%d %s ", 1, "a")
	Warn("warm test.%d %s ", 1, "a")
	Error("error test.%d %s ", 1, "a")
	Fatal("fatal test.%d %s ", 1, "a")
}
