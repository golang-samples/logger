package logger

type LogData struct {
	Message     string
	TimeString  string
	LevelString string
	FileName    string
	FuncName    string
	LineNum     int
	WarnAndFatal bool
}
