package logger

import (
	"fmt"
	"os"
	// "path"
	// "time"
)

type ConsoleLogger struct {
	level int
}

func NewConsoleLogger(config map[string]string) (logger Logger, err error) {
	logLevel, ok := config["log_level"]
	if !ok {
		err = fmt.Errorf("not found log_level")
		return
	}
	level := getLogLevel(logLevel)
	c := &ConsoleLogger{
		level: level,
	}
	c.Init()
	logger = c
	return
}

func (c *ConsoleLogger) Init() {
	c.SetLevel(c.level)
}

func (c *ConsoleLogger) SetLevel(level int) {
	if c.level < LogLevelDebug || c.level > LogLevelFatal {
		c.level = LogLevelDebug
	}
}

func (c *ConsoleLogger) GetLevel() int {
	return c.level
}

func (c *ConsoleLogger) Debug(format string, args ...interface{}) {
	if c.level > LogLevelDebug {
		return
	}
	logData := writeLog(LogLevelDebug, format, args...)
	fmt.Fprintf(os.Stdout, "%s [%s] (%s %s:%d) %s\n", logData.TimeString, logData.LevelString,
		logData.FileName, logData.FuncName, logData.LineNum, logData.Message)
}

func (c *ConsoleLogger) Trace(format string, args ...interface{}) {
	if c.level > LogLevelTrace {
		return
	}
	logData := writeLog(LogLevelTrace, format, args...)
	fmt.Fprintf(os.Stdout, "%s [%s] (%s %s:%d) %s\n", logData.TimeString, logData.LevelString,
		logData.FileName, logData.FuncName, logData.LineNum, logData.Message)
}

func (c *ConsoleLogger) Info(format string, args ...interface{}) {
	if c.level > LogLevelInfo {
		return
	}
	logData := writeLog(LogLevelInfo, format, args...)
	fmt.Fprintf(os.Stdout, "%s [%s] (%s %s:%d) %s\n", logData.TimeString, logData.LevelString,
		logData.FileName, logData.FuncName, logData.LineNum, logData.Message)
}

func (c *ConsoleLogger) Warn(format string, args ...interface{}) {
	if c.level > LogLevelWarn {
		return
	}
	logData := writeLog(LogLevelWarn, format, args...)
	fmt.Fprintf(os.Stdout, "%s [%s] (%s %s:%d) %s\n", logData.TimeString, logData.LevelString,
		logData.FileName, logData.FuncName, logData.LineNum, logData.Message)
}

func (c *ConsoleLogger) Error(format string, args ...interface{}) {
	if c.level > LogLevelError {
		return
	}
	logData := writeLog(LogLevelError, format, args...)
	fmt.Fprintf(os.Stdout, "%s [%s] (%s %s:%d) %s\n", logData.TimeString, logData.LevelString,
		logData.FileName, logData.FuncName, logData.LineNum, logData.Message)
}

func (c *ConsoleLogger) Fatal(format string, args ...interface{}) {
	if c.level > LogLevelFatal {
		return
	}
	logData := writeLog(LogLevelFatal, format, args...)
	fmt.Fprintf(os.Stdout, "%s [%s] (%s %s:%d) %s\n", logData.TimeString, logData.LevelString,
		logData.FileName, logData.FuncName, logData.LineNum, logData.Message)
}

func (c *ConsoleLogger) Close() {

}
